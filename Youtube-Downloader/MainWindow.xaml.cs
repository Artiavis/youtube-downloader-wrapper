﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace Youtube_Downloader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DestinationFolder = Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
            DestinationFolderTextBlock.Text = DestinationFolder;
        }

        private string DestinationFolder { get; set; }
        private YoutubeDownloadWrapper.DownloadAction _previousDownloadAction = YoutubeDownloadWrapper.DownloadAction.DownloadVideo;

        private void OnKeyDownHandler(object sender, KeyEventArgs e) 
        {
            if (e.Key == Key.Return)
            {
                BeginDownloadAction(_previousDownloadAction);
            }
        }


        private void DownloadVideoButton_OnClick(object sender, RoutedEventArgs e)
        {
            BeginDownloadAction(YoutubeDownloadWrapper.DownloadAction.DownloadVideo);
            _previousDownloadAction = YoutubeDownloadWrapper.DownloadAction.DownloadVideo;
        }

        private void DownloadAudioButton_OnClick(object sender, RoutedEventArgs e)
        {
            BeginDownloadAction(YoutubeDownloadWrapper.DownloadAction.DownloadAudio);
            _previousDownloadAction = YoutubeDownloadWrapper.DownloadAction.DownloadAudio;
        }

        private void BeginDownloadAction(YoutubeDownloadWrapper.DownloadAction downloadAction)
        {
            var urlString = UrlBox.Text.Trim();
            if (Uri.IsWellFormedUriString(urlString, UriKind.Absolute))
            {
                BeginDownload(urlString, downloadAction);
            }
            else
            {
                DownloadProgressBar.Value = DownloadProgressBar.Minimum;
            }
        }

        /// <summary>
        /// Disable or enable inputs during a blocking UI action.
        /// </summary>
        /// <param name="enabled"></param>
        private void InputsEnabled(bool enabled)
        {
            UrlBox.IsEnabled = enabled;

            DestFolderSelectButton.IsEnabled = enabled;
            DownloadVideoButton.IsEnabled = enabled;
            DownloadAudioButton.IsEnabled = enabled;

            IsCustomFilenameCheckBox.IsEnabled = enabled;

            if (enabled)
            {
                var binding = new Binding
                {
                    Source = IsCustomFilenameCheckBox,
                    Path = new PropertyPath(ToggleButton.IsCheckedProperty)
                };
                BindingOperations.SetBinding(CustomFileNameTextBox, IsEnabledProperty, binding);
            }
            else
            {
                CustomFileNameTextBox.IsEnabled = false;
            }
        }

        private void BeginDownload(string urlString, YoutubeDownloadWrapper.DownloadAction downloadAction)
        {
            InputsEnabled(false);

            YoutubeDownloadWrapper downloader;


            var hasCustomFileName = IsCustomFilenameCheckBox.IsChecked.Value;
            var fileName = CustomFileNameTextBox.Text;

            if (hasCustomFileName && !String.IsNullOrWhiteSpace(fileName))
                downloader = new YoutubeDownloadWrapper(DestinationFolder, fileName);
            else
                downloader = new YoutubeDownloadWrapper(DestinationFolder);
            
            downloader.Exited += (sender, args) =>
            {
                Dispatcher.Invoke(() =>
                {
                    //                        StatusBox.Text = "";
                    UrlBox.Clear();
                    CustomFileNameTextBox.Clear();
                    DownloadProgressBar.Value = DownloadProgressBar.Maximum;
                    InputsEnabled(true);
                });
                downloader.Dispose();
            };
            downloader.ErrorDataReceived += (sender, args) =>
            {
                if (args.Data == null) return;
                Dispatcher.InvokeAsync(() =>
                {
                    StatusBox.Text += (args.Data + "\n");
                    StatusBoxScroller.ScrollToBottom();
                });
            };
            const string percentRegexString = @"(\d+[.]?\d*)%";
            var percentRegex = new Regex(percentRegexString);
            downloader.OutputDataReceived += (sender, args) =>
            {
                if (args.Data == null) return;
                Dispatcher.InvokeAsync(() =>
                {
                    StatusBox.Text += (args.Data + "\n");
                    StatusBoxScroller.ScrollToBottom();
                });
                var match = percentRegex.Match(args.Data);
                if (!match.Success) return;
                var capture = match.Groups[1].Captures[0].ToString();
                var percentage = Double.Parse(capture);
                Dispatcher.InvokeAsync(() => { DownloadProgressBar.Value = percentage * DownloadProgressBar.Maximum; });
            };

            StatusBox.Text = "";
  
            var command = downloader.BeginDownload(urlString, downloadAction);
            StatusBox.Text += (command + "\n");
        }

        private void DestFolderSelectButton_OnClick(object sender, RoutedEventArgs e)
        {
            DestinationFolder = PickFolder();
            DestinationFolderTextBlock.Text = DestinationFolder;
        }

        /// <summary>
        /// Open a dialog to pick a folder and return the selected value.
        /// </summary>
        /// <returns>A string with the selected value, or null.</returns>
        private static string PickFolder()
        {
            var dlg = new CommonOpenFileDialog
            {
                Title = "Pick a Destination Folder",
                IsFolderPicker = true,
                AddToMostRecentlyUsedList = false,
                AllowNonFileSystemItems = false,
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                Multiselect = false,
                ShowPlacesList = true
            };

            return dlg.ShowDialog() == CommonFileDialogResult.Ok ? dlg.FileName : null;
        }


    }
}
