﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Youtube_Downloader
{
    class YoutubeDownloadWrapper : IDisposable
    {
        public YoutubeDownloadWrapper(string workingDirectory = null, string outputFile = null)
        {
            SetDirectoryAndOutputFile(workingDirectory, outputFile);
        }

        private void SetDirectoryAndOutputFile(string workingDirectory = null, string outputFile = null)
        {
            // Ensure valid potential filename
            // If absolute path, try and set both output file and folder
            if (!String.IsNullOrWhiteSpace(outputFile) && Path.IsPathRooted(outputFile))
            {
                SetAbsolutePath(outputFile);
                return;
            }

            // If not absolute path, check for relative path compred to workingDirectory
            if (!String.IsNullOrWhiteSpace(workingDirectory)
                && !String.IsNullOrEmpty(Path.GetDirectoryName(outputFile)))
            {
                SetRelativePath(workingDirectory, outputFile);
                return;
            }

            // If not absolute or relative path, assume in given directory
            if (workingDirectory == null || Directory.Exists(workingDirectory))
            {
                WorkingDirectory = workingDirectory;
            }
            else
            {
                throw new DirectoryNotFoundException("Folder does not exist!");
            }

            OutputFile = String.IsNullOrWhiteSpace(outputFile) ? null : outputFile;
        }

        /// <summary>
        /// Sets the <see cref="WorkingDirectory"/> and <see cref="OutputFile"/>
        /// properties, given a single, absolute path to either a file in an existing folder
        /// or an existing folder. If <paramref name="fullPath"/> refers to a directory 
        /// and not a file, the file name will be set to null.
        /// </summary>
        /// <param name="fullPath">Either an absolute path to an existing folder, 
        /// or an absolute path to a to-be-created file in an existing folder.</param>
        private void SetAbsolutePath(string fullPath)
        {

            if (!Path.IsPathRooted(fullPath))
            {
                throw new FileFormatException();
            }

            fullPath = Path.GetFullPath(fullPath);

            // Check that the "full path" doesn't name/terminate on a folder
            // If it does, assume default file name via null.
            if (Directory.Exists(fullPath))
            {
                WorkingDirectory = fullPath;
                OutputFile = null;
                return;
            }

            // Otherwise, check that the full path names a folder and a file
            var dirName = Path.GetDirectoryName(fullPath);
            if (dirName != null && Directory.Exists(dirName))
            {
                WorkingDirectory = dirName;
                OutputFile = Path.GetFileName(fullPath);
            }
            else
            {
                throw new DirectoryNotFoundException("The specified path contains an invalid folder name!");
            }
        }

        /// <summary>
        /// Sets the <see cref="WorkingDirectory"/> and <see cref="OutputFile"/> properties
        /// by finding combining <paramref name="directory"/> with <paramref name="relativePath"/>.
        /// If these paths combine to name a directory, the file name will be set to null.
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="relativePath"></param>
        private void SetRelativePath(string directory, string relativePath)
        {
            if (directory == null || relativePath == null)
            {
                throw new FileFormatException("The specified path contains an invalid folder name!");
            }
            if (File.Exists(directory))
            {
                throw new FileFormatException("Cannot specify a file as a directory!");
            }
            
            // Check if a path to an existing directory
            var fullPath = Path.GetFullPath(Path.Combine(directory, relativePath));
            if (Directory.Exists(fullPath))
            {
                WorkingDirectory = fullPath;
                OutputFile = null;
                return;
            }

            // Otherwise, assume path to file
            var relDir = Path.GetDirectoryName(fullPath);
            var fileName = Path.GetFileName(fullPath);

            if (relDir == null || !Directory.Exists(relDir))

                throw new FileFormatException("The specified path contains an invalid folder name!");

            WorkingDirectory = relDir;
            OutputFile = fileName;
        }

        public enum DownloadAction
        {
            DownloadAudio, DownloadVideo
        }

        public string WorkingDirectory { get; set; }
        public string OutputFile { get; set; }

        public event EventHandler Exited;
        public event DataReceivedEventHandler ErrorDataReceived;
        public event DataReceivedEventHandler OutputDataReceived;
        private Process Process { get; set; }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (Process == null) return;
            Process.Dispose();
            Process = null;
        }


        public string BeginDownload(string urlString, DownloadAction downloadAction)
        {
            if (!Uri.IsWellFormedUriString(urlString, UriKind.Absolute))
                throw new UriFormatException();

            var dir = WorkingDirectory ?? Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
            var fileName = OutputFile ?? @"%(title)s-%(id)s.%(ext)s";
            var outputFileName = Path.Combine(dir, fileName);

            var command = "youtube-dl " + urlString + " --output " + outputFileName;

            if (downloadAction == DownloadAction.DownloadAudio)
                command += " --extract-audio";

            var process = new Process
            {
                EnableRaisingEvents = true,
                StartInfo = new ProcessStartInfo
                {
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "cmd.exe",
                    WorkingDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                    Arguments = "/C " + command
                }
            };

            
            process.Exited += Exited;
            process.OutputDataReceived += OutputDataReceived;
            process.ErrorDataReceived += ErrorDataReceived;



            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();

            Process = process;

            return command;
        }


    }
}
